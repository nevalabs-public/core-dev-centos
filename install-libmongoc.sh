#! /bin/bash
set -e # fail fast

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

LIBMONGOC_VERS=1.15.0

pushd /tmp
if [ ! -f "mongo-c-driver-${LIBMONGOC_VERS}.tar.gz" ]; then
    curl -OLsS https://github.com/mongodb/mongo-c-driver/releases/download/${LIBMONGOC_VERS}/mongo-c-driver-${LIBMONGOC_VERS}.tar.gz
fi
tar xzf mongo-c-driver-${LIBMONGOC_VERS}.tar.gz
pushd mongo-c-driver-${LIBMONGOC_VERS}
mkdir -p cmake-build
pushd cmake-build
# mongo-c-driver contains a copy of libbson, in case your system does not already have libbson installed. 
# The build will detect if libbson is not installed and use the bundled libbson
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_AUTOMATIC_INIT_AND_CLEANUP=OFF ..
make && make install
popd
popd
popd

rm -rf /tmp/mongo*
