#! /bin/bash
set -e # fail fast

# download boost
BOOST_VERS=1.83.0
pushd /tmp
curl -SL "http://downloads.sourceforge.net/project/boost/boost/${BOOST_VERS}/boost_1_83_0.tar.bz2" -o boost.tar.bz2
[ $(sha1sum boost.tar.bz2 | awk '{print $1}') == 'b6b284acde2ad7ed49b44e856955d7b1ea4e9459' ]

# configure boost
tar --bzip2 -xf boost.tar.bz2
echo "tardan cikarildi"
pushd boost_1_83_0
chmod -R 777 *
echo "yetkiler verildi"
#sed -i 's/which/command -v/' bootstrap.sh
#echo "whichler değiştirildi"
./bootstrap.sh --prefix=/usr/local --without-libraries=python
echo "boststrap.sh çalistirildi"
# build & install boost
./b2 -a -sHAVE_ICU=1 -j4 --build-type=complete --layout=versioned address-model=64 cxxflags="-fPIC -std=c++14" linkflags="-fPIC"
./b2 install
echo "b2 yuklendi"
popd
popd

rm -rf /tmp/boost*
echo "/usr/local/lib" >> /etc/ld.so.conf.d/boost-1.83.0.conf