#! /bin/bash
set -e # fail fast

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

MONGOCXX_VERS=3.4.0

pushd /tmp
if [ ! -f "r${MONGOCXX_VERS}.tar.gz" ]; then
    curl -OL https://github.com/mongodb/mongo-cxx-driver/archive/r${MONGOCXX_VERS}.tar.gz
fi
tar -xzf r${MONGOCXX_VERS}.tar.gz
pushd mongo-cxx-driver-r${MONGOCXX_VERS}/build
cmake -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/usr/local \
  -DCMAKE_PREFIX_PATH==/usr/local \
  -DBSONCXX_POLY_USE_BOOST=1 ..
make && make install
popd
popd

rm -rf /tmp/mongo*

# find_package(LIBMONGOCXX 3.4.0 REQUIRED);
#   LIBMONGOCXX_INCLUDE_DIRS
#   LIBMONGOCXX_LIBRARIES