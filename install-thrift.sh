#! /bin/bash
set -e # fail fast

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

THRIFT_VERS=0.11.0

add_boost_lib64_link()
{
    if [ $(find /usr/local/lib64/ -name 'libboost*' | wc -c) == 0 ]; then
        echo "making boost lib64 symbolic links"
        ln -s /usr/local/lib/libboost* /usr/local/lib64
    fi
}

remove_boost_lib64_link()
{
    echo "removing boost lib64 symbolic links"
    rm -f /usr/local/lib64/libboost*
}

pushd /tmp
# download
if [ ! -f thrift-${THRIFT_VERS}.tar.gz ]; then
    curl -SLO "http://www-eu.apache.org/dist/thrift/${THRIFT_VERS}/thrift-${THRIFT_VERS}.tar.gz";
    [ $(md5sum thrift-${THRIFT_VERS}.tar.gz | awk '{print $1}') == '0be59730ebce071eceaf6bfdb8d3a20e' ]
fi
tar xvf thrift-${THRIFT_VERS}.tar.gz
pushd thrift-${THRIFT_VERS}

# build & install
./bootstrap.sh
# TODO: -DT_GLOBAL_LOGGING_LEVEL=0
./configure --prefix=/usr/local --without-java --without-python --with-boost=/usr/local CXXFLAGS='-m64 -g -O2'
add_boost_lib64_link
make
make check
remove_boost_lib64_link
make install
popd
popd

